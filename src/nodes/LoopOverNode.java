package nodes;

public class LoopOverNode extends TreeNode {

	@Override
	public Object execute() {
		//System.out.println("Executing Loop Over node");
		
		String name = (String) this.children.get(0).execute();
		if (AssignNode.variables.containsKey(name)) {
			VariableType var = AssignNode.variables.get(name);
			if (var.array != null) {
				if (this.children.size() != 2) {
					String name2 = (String) this.children.get(1).execute();
					VariableType temp = null;
					for (Object it : var.array) {
						temp = new VariableType(var.type, it);
						AssignNode.variables.put(name2, temp);
						this.children.get(2).execute();
					}
					AssignNode.variables.remove(temp);
				} else {
					for (Object it : var.array) {
						var.value = it;
						this.children.get(1).execute();
					}
					var.value = null;
				}
			}
		}

		return null;
	}

}
