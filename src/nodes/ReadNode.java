package nodes;

import java.util.Scanner;

public class ReadNode extends TreeNode {

	@Override
	public Object execute() {
		//System.out.println("Executing Read node");
		
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		return input;
	}

}
